//
//  Graduate.swift
//  Association
//
//  Created by MacBook Pro on 13.06.2022.
//

import Foundation

struct Graduate {
    let name: String
    let birthday: String
    let iin: String
    let group: String
    let endYear: String
    let address: String
    let work: String?
    let phone: String
    let familyStatus: String
}

var graduates: [Graduate] = []
