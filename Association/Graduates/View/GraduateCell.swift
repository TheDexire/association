//
//  GraduateCell.swift
//  Association
//
//  Created by MacBook Pro on 13.06.2022.
//

import UIKit

class GraduateCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var iinLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with graduate: Graduate) {
        nameLabel.text = graduate.name
        birthdayLabel.text = graduate.birthday
        iinLabel.text = graduate.iin
    }
}
