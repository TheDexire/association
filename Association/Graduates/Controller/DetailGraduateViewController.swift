//
//  DetailGraduateViewController.swift
//  Association
//
//  Created by MacBook Pro on 14.06.2022.
//

import UIKit

class DetailGraduateViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var iinLabel: UILabel!
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var endYearLabel: UILabel!
    @IBOutlet weak var addtressLabel: UILabel!
    @IBOutlet weak var workLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var familyStatusLabel: UILabel!
    
    var graduate: Graduate?

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let graduate = graduate else { return }
        setGraduate(with: graduate)
    }
    
    func setGraduate(with data: Graduate) {
        let name = "ФИО: " + data.name
        let birthday = "Дата рождения: " + data.birthday
        let iin = "ИИН: " + data.iin
        let group = "Группа: " + data.group
        let endYear = "Год выпуска: " + data.endYear
        let addtress = "Адрес проживания: " + data.address
        let work = "Место работы: " + (data.work ?? "")
        let phone = "Номер телефона: " + data.phone
        let familyStatus = "Семейное положение: " + data.familyStatus
        
        nameLabel.attributedText = customizeFont(for: name, length: 4)
        birthdayLabel.attributedText = customizeFont(for: birthday, length: 14)
        iinLabel.attributedText = customizeFont(for: iin, length: 4)
        groupLabel.attributedText = customizeFont(for: group, length: 7)
        endYearLabel.attributedText = customizeFont(for: endYear, length: 12)
        addtressLabel.attributedText = customizeFont(for: addtress, length: 17)
        workLabel.attributedText = customizeFont(for: work, length: 13)
        phoneLabel.attributedText = customizeFont(for: phone, length: 15)
        familyStatusLabel.attributedText = customizeFont(for: familyStatus, length: 19)
    }
    
    func customizeFont(for string: String, start: Int = 0, length: Int) -> NSMutableAttributedString {
        let myMutableString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font :UIFont.systemFont(ofSize: 18, weight: .medium)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 0.251, green: 0.314, blue: 0.694, alpha: 1.0), range: NSRange(location:start,length:length))
        return myMutableString
    }

}
