//
//  EditGraduateViewController.swift
//  Association
//
//  Created by MacBook Pro on 14.06.2022.
//

import UIKit
import CoreData

class EditGraduateViewController: UIViewController {
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var birthdayField: UITextField!
    @IBOutlet weak var iinField: UITextField!
    @IBOutlet weak var groupField: UITextField!
    @IBOutlet weak var endYearField: UITextField!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var workField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var familyStatusField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func doneTapped() {
        if isFormFilled() {
            createGraduate()
        } else {
            showAlert(message: "Заполните все поля")
        }
    }
    
    private func createGraduate() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let graduateEntity = NSEntityDescription.entity(forEntityName: "Graduates", in: managedContext)!
        
        let graduate = NSManagedObject(entity: graduateEntity, insertInto: managedContext)
        graduate.setValue(1, forKeyPath: "id")
        graduate.setValue(nameField.text, forKey: "name")
        graduate.setValue(iinField.text, forKey: "iin")
        graduate.setValue(phoneField.text, forKey: "phone")
        graduate.setValue(workField.text, forKey: "work")
        graduate.setValue(groupField.text, forKey: "group")
        graduate.setValue(familyStatusField.text, forKey: "familyStatus")
        graduate.setValue(endYearField.text, forKey: "endYear")
        graduate.setValue(birthdayField.text, forKey: "birthday")
        graduate.setValue(addressField.text, forKey: "address")
        
        do {
            try managedContext.save()
            navigationController?.popViewController(animated: true)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    private func isFormFilled() -> Bool {
        if !nameField.text!.isEmpty && !birthdayField.text!.isEmpty && !iinField.text!.isEmpty
            && !groupField.text!.isEmpty && !endYearField.text!.isEmpty && !addressField.text!.isEmpty
            && !workField.text!.isEmpty && !phoneField.text!.isEmpty && !familyStatusField.text!.isEmpty {
            return true
        }
        return false
    }

}
