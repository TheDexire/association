//
//  GraduateController.swift
//  Association
//
//  Created by MacBook Pro on 13.06.2022.
//

import UIKit
import CoreData

class GraduateController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var plusButton: UIBarButtonItem!
    @IBOutlet weak var emptyLabel: UILabel!
    
    var managedObjectContext: NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "GraduateCell", bundle: nil), forCellReuseIdentifier: "graduate_cell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getGraduates()
        if UserDefaults.standard.string(forKey: "userRole") == "student" {
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    @IBAction func addTapped() {
        performSegue(withIdentifier: "editSegue", sender: nil)
    }
    
    func removeGraduate(iin: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Graduates")
        fetchRequest.predicate = NSPredicate(format: "iin = %@", iin)
        
        do {
            let test = try managedContext.fetch(fetchRequest)
            
            let objectToDelete = test[0] as! NSManagedObject
            managedContext.delete(objectToDelete)
            
            do{
                try managedContext.save()
            }
            catch {
                print(error)
            }
        }
        catch {
            print(error)
        }
    }
    
    func getGraduates() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext =
        appDelegate.persistentContainer.viewContext
        let fetchRequest =
        NSFetchRequest<NSManagedObject>(entityName: "Graduates")
        
        do {
            let graduatesData = try managedContext.fetch(fetchRequest)
            graduates.removeAll()
            for graduate in graduatesData {
                let name = graduate.value(forKey: "name") as! String
                let birthday = graduate.value(forKey: "birthday") as! String
                let iin = graduate.value(forKey: "iin") as! String
                let group = graduate.value(forKey: "group") as! String
                let endYear = graduate.value(forKey: "endYear") as! String
                let work = graduate.value(forKey: "work") as! String
                let address = graduate.value(forKey: "address") as! String
                let phone = graduate.value(forKey: "phone") as! String
                let familyStatus = graduate.value(forKey: "familyStatus") as! String
                graduates.append(Graduate(name: name, birthday: birthday, iin: iin, group: group, endYear: endYear, address: address, work: work, phone: phone, familyStatus: familyStatus))
            }
            if graduates.count == 0 {
                self.emptyLabel.isHidden = false
            } else {
                self.emptyLabel.isHidden = true
            }
            self.tableView.reloadData()
        } catch let error as NSError {
            print("Произошла ошибка \(error)")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return graduates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "graduate_cell", for: indexPath) as? GraduateCell else { return UITableViewCell() }
        let graduate = graduates[indexPath.row]
        cell.configure(with: graduate)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailGraduateSegue", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return UserDefaults.standard.string(forKey: "userRole") == "admin"
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            removeGraduate(iin: graduates[indexPath.row].iin)
            graduates.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            if graduates.count == 0 {
                self.emptyLabel.isHidden = false
            } else {
                self.emptyLabel.isHidden = true
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Удалить"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailGraduateSegue" {
            if let indexPath = tableView.indexPathForSelectedRow {
                guard let detailController = segue.destination as? DetailGraduateViewController else { return }
                let graduate = graduates[indexPath.row]
                detailController.graduate = graduate
            }
            
        }
    }
}
