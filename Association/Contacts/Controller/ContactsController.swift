//
//  ContactsController.swift
//  Association
//
//  Created by MacBook Pro on 13.06.2022.
//

import UIKit

class ContactsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ContactCell", bundle: nil), forCellReuseIdentifier: "contact_cell")
    }
    
    @IBAction func logoutTapped() {
        UserDefaults.standard.removeObject(forKey: "userRole")
        performSegue(withIdentifier: "loginSegue", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "contact_cell", for: indexPath) as? ContactCell else { return UITableViewCell() }
        let contact = contacts[indexPath.row]
        cell.configure(with: contact)
        return cell
    }
}
