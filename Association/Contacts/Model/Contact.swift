//
//  Contact.swift
//  Association
//
//  Created by MacBook Pro on 13.06.2022.
//

import Foundation

struct Contact {
    let name: String
    let phone: String
    let email: String
}

let contacts: [Contact] = [.init(name: "Приемная комиссия", phone: "+7 (7172) 39 51 10, +7 702 912 39 97, +7 705 153 29 53, +7 702 671 43 90, +7 701 158 89 24, +7 701 772 39 94, +7 707 121 33 83, +7 702 623 75 79", email: "admissions@tau-edu.kz"),
                           .init(name: "Центр обслуживания студентов", phone: "+7 (7172) 39-81-18 (вн.124); +7 701 181 90 71", email: "gribincha@tau-edu.kz"),
                           .init(name: "Центр дистанционных технологий обучения", phone: "+7 (7172) 39-81-18 (вн. 113), 8 701 451 56 52", email: "zhusupova@tau-edu.kz"),
                           .init(name: "Отдел международного сотрудничества и академической мобильности", phone: "+7 (7172) 39 51 10", email: "aitzhanova@tau-edu.kz"),
                           .init(name: "Приёмная ректора, канцелярия", phone: "+7 (7172) 39 81 18, 27 80 87", email: "info@tau-edu.kz"),
                           .init(name: "HR-служба", phone: "+7 (7172) 39 81 18, вн. 129", email: "cv@tau-edu.kz"),
                           .init(name: "Бахгалтерия", phone: "+7 (7172) 27 80 39, 39 68 89", email: "amrenova.laila@mail.ru"),
                           .init(name: "Деканат гуманитарно-юридического факультета", phone: "+7 (7172) 39-73-39", email: "m.narbinova@tau-edu.kz"),
                           .init(name: "Деканат факультета Бизнес и информационные технологии", phone: "+7 (7172) 31-11-48", email: "g.esenova@tau-edu.kz"),
                           .init(name: "Отдел послевузовского образования (магистратура, докторантура)", phone: "+7 (7172) 39 81 18, вн. 127", email: "anafina@tau-edu.kz"),
                           .init(name: "Департамент по академическим вопросам", phone: "+7 7172 39 81 18, вн. 125", email: "g.rahimova@tau-edu.kz, svet-30@inbox.ru"),
                           .init(name: "Отдел науки, управления качеством, аккредитации и рейтинга", phone: "-", email: "accreditation@tau-edu.kz"),
                           .init(name: "Отдел практики, трудоустройства и карьеры", phone: "+7 (7172) 39-81-18 (вн. 126), 8 707 770 13 14", email: "x.tabeshev@tau-edu.kz"),
                           .init(name: "Комитет по делам молодежи", phone: "+7 7172 39 8118 (вн. 114), 8 702 671 43 90", email: "kairzhanov@tau-edu.kz"),
                           .init(name: "Библиотечно-информационный центр", phone: "+7 (7172) 39 81 18 (вн. 110)", email: "sh.taubaeva@tau-edu.kz"),
                           .init(name: "Типография", phone: "+7 (7172) 39 81 18, вн. 114", email: "kanat67@mail.ru, mansurov@tau-edu.kz"),
                           .init(name: "Профсоюз", phone: "+7 (7172) 39 8118, вн. 125", email: "svet-30@inbox.ru"),
                           .init(name: "Медпункт", phone: "+7 (7172) 39 81 18, вн. 109", email: "med.punkt@ tau-edu.kz"),
                           .init(name: "Департамент науки и инноваций", phone: "+7 (7172) 39 81 18, вн. 108", email: "smoilovs@mail.ru")
]
