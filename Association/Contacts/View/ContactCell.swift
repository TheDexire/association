//
//  ContactCell.swift
//  Association
//
//  Created by MacBook Pro on 13.06.2022.
//

import UIKit

class ContactCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(with contact: Contact) {
        nameLabel.text = contact.name
        phoneLabel.text = "Тел.: " + contact.phone
        emailLabel.text = "E-mail: " + contact.email
    }
    
}
