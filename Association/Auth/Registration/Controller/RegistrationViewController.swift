//
//  RegistrationViewController.swift
//  Association
//
//  Created by MacBook Pro on 13.06.2022.
//

import UIKit
import CoreData

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginTapped() {
        performSegue(withIdentifier: "loginSegue", sender: nil)
    }
    
    @IBAction func registerTapped() {
        if isFormFilled() {
            createUser()
        } else {
            showAlert(message: "Заполните все поля")
        }
    }
    
    private func createUser() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let graduateEntity = NSEntityDescription.entity(forEntityName: "Users", in: managedContext)!
        
        let graduate = NSManagedObject(entity: graduateEntity, insertInto: managedContext)
        graduate.setValue(nameField.text, forKey: "name")
        graduate.setValue(loginField.text, forKey: "login")
        graduate.setValue(passwordField.text, forKey: "password")
        
        do {
            try managedContext.save()
            performSegue(withIdentifier: "loginSegue", sender: nil)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

    private func isFormFilled() -> Bool {
        if !nameField.text!.isEmpty && !loginField.text!.isEmpty && !passwordField.text!.isEmpty {
            return true
        }
        return false
    }
}
