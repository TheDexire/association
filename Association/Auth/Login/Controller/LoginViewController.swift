//
//  LoginViewController.swift
//  Association
//
//  Created by MacBook Pro on 13.06.2022.
//

import UIKit
import CoreData

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    let adminLogin = (login: "admin", password: "admin")

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginTapped() {
        if loginField.text == adminLogin.login && passwordField.text == adminLogin.password {
            UserDefaults.standard.set("admin", forKey: "userRole")
            performSegue(withIdentifier: "mainAppSegue", sender: nil)
        } else if isAccessGraduate(login: loginField.text!, password: passwordField.text!) {
            UserDefaults.standard.set("student", forKey: "userRole")
            performSegue(withIdentifier: "mainAppSegue", sender: nil)
        } else {
            showAlert(message: "Неверный логин или пароль")
        }
    }
    
    @IBAction func registrationTapped() {
        performSegue(withIdentifier: "registrationSegue", sender: nil)
    }
    
    private func isAccessGraduate(login: String, password: String) -> Bool {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return false }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        fetchRequest.predicate = NSPredicate(format: "login = %@", login)
        
        do {
            let request = try managedContext.fetch(fetchRequest)
            guard !request.isEmpty, let object = request[0] as? NSManagedObject, let userPassword = object.value(forKey: "password") as? String else { return false }
            if password == userPassword {
                return true
            }
        }
        catch {
            print(error)
            return false
        }
        return false
    }

}

extension UIViewController {
    func showAlert(message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
